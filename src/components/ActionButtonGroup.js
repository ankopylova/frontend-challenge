import React, { useMemo } from 'react';
import { connect } from 'react-redux';
import { addQuantity, addToCart, removeItem, subtractQuantity } from '../reducers/actions';

const ActionButtonGroup = ({
  id,
  count,
  addQuantity,
  addToCart,
  removeItem,
  subtractQuantity,
  products,
}) => {

  const handleRemoveItem = () => {
    subtractQuantity(id);
    if (count === 1) removeItem(id);
  };

  const lastAddedItemTitle = useMemo(() => products.find(el => el.id === id).title, [products, id]);

  const handleAddItem = () => {
    addQuantity(id);
    const toastHTML = `
      <div> 
        <span>'${lastAddedItemTitle}' is added to your cart</span>
      </div>
    `;
    M.toast({ html: toastHTML, classes: 'custom-toast' });
    if (!count) addToCart(id);
  };

  return (
    <>
      {!!count &&
        <>
          <button className="btn-floating waves-effect waves-light red"
            onClick={() => handleRemoveItem()}
          >
            <i className="material-icons">remove</i>
          </button>
          <span className="count">{count}</span>
        </>
      }
      <button className="btn-floating waves-effect waves-light red"
        onClick={() => handleAddItem()}
      >
        <i className="material-icons">add</i>
      </button>
    </>
  )
};

const mapStateToProps = state => ({
  products: state.products,
  myCart: state.myCart,
});

const mapDispatchToProps = dispatch => ({
  addToCart: id => dispatch(addToCart(id)),
  addQuantity: id => dispatch(addQuantity(id)),
  removeItem: id => dispatch(removeItem(id)),
  subtractQuantity: id => dispatch(subtractQuantity(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ActionButtonGroup);
