import React from "react";
import { connect } from 'react-redux';
import { totalItems } from '../helpers/total';

const TotalItemsCount = ({ myCart }) => {
  if (myCart) return (
    <span className="total-items white-text">
      {totalItems(myCart)}
    </span>
  );

  return null;
};

const mapStateToProps = state => ({
  myCart: state.myCart,
});

export default connect(mapStateToProps)(TotalItemsCount);
