import React from 'react';
import ActionButtonGroup from './ActionButtonGroup';

const Card = ({ img, title, desc, price, count, id }) => (
  <div className="card">
    <div className="card-image">
      <img src={img} alt={title} />
      <span className="card-title truncate">{title}</span>
    </div>
    <div className="count-container">
      <ActionButtonGroup count={count} id={id} />
    </div>
    <div className="card-content">
      <p>{desc}</p>
      <p>
        <b>
          Price: {price}$
        </b>
      </p>
    </div>
  </div>
);

export default Card;
