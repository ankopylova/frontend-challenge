import React from 'react';
import { Link } from 'react-router-dom';
import TotalItemsCount from './TotalItemsCount';

const Navbar = () => (
  <nav className="nav-wrapper">
    <div className="container">
      <Link to='/' className="brand-logo">Shopping</Link>
      <ul className="right">
        <li className="nav-link">
          <Link to='/'>Shop</Link>
        </li>
        <li className="nav-link">
          <Link to='/cart'>My cart </Link>
        </li>
        <li>
          <Link to='/cart'>
            <i className="material-icons cart-icon">shopping_cart</i>
          </Link>
        </li>
        <li>
          <TotalItemsCount />
        </li>
      </ul>
    </div>
  </nav>

);

export default Navbar;
