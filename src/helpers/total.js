import { useMemo } from 'react';

export const totalOrderAmount = (items, shipping) => (
  items?.length ?
    useMemo(() => items?.reduce((a, c) => a + (c.count * c.price), shipping), [items]) :
    null
);

export const totalItems = (items) => (
  items?.length ?
    useMemo(() => items.reduce((a, c) => a + c.count, 0), [items]) :
    null
);
