import Item1 from '../images/item1.jpg';
import Item2 from '../images/item2.jpg';
import Item3 from '../images/item3.jpg';
import Item4 from '../images/item4.jpg';
import Item5 from '../images/item5.jpg';
import Item6 from '../images/item6.jpg';
import {
  ADD_QUANTITY,
  ADD_TO_CART,
  REMOVE_ITEM,
  SUB_QUANTITY,
} from './constants';


const initialState = {
  products: [
    {
      id: 1,
      title: 'Winter body',
      desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.',
      price: 110,
      img: Item1,
      count: 0,
    },
    {
      id: 2,
      title: 'Adidas',
      desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.',
      price: 80,
      img: Item2,
      count: 0,
    },
    {
      id: 3,
      title: 'Vans',
      desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.',
      price: 120,
      img: Item3,
      count: 0,
    },
    {
      id: 4,
      title: 'White',
      desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.',
      price: 260,
      img: Item4,
      count: 0,
    },
    {
      id: 5,
      title: 'Cropped-sho',
      desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.',
      price: 160,
      img: Item5,
      count: 0,
    },
    {
      id: 6,
      title: 'Blues',
      desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.',
      price: 90,
      img: Item6,
      count: 0,
    },
  ],
  myCart: [],
  shipping: 20,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_QUANTITY:
      return {
        ...state,
        products: state.products.map(el =>
          el.id === action.payload ? { ...el, count: el.count + 1 } : el
        ),
        myCart: state.myCart.map(el =>
          el.id === action.payload ? { ...el, count: el.count + 1 } : el
        )
      }
    case ADD_TO_CART:
      return {
        ...state,
        myCart: [...state.myCart, state.products.find(el => el.id === action.payload)],
      }
    case SUB_QUANTITY:
      return {
        ...state,
        products: state.products.map(el =>
          el.id === action.payload ? { ...el, count: el.count - 1 } : el
        ),
        myCart: state.myCart.map(el =>
          el.id === action.payload ? { ...el, count: el.count - 1 } : el
        )
      }
    case REMOVE_ITEM:
      return {
        ...state,
        products: state.products.map(el =>
          el.id === action.payload ? { ...el, count: 0 } : el
        ),
        myCart: state.myCart.filter(el => el.id !== action.payload),
      }
    default:
      return state;
  }

};
