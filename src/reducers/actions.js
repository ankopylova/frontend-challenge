import {
  ADD_QUANTITY,
  ADD_TO_CART,
  REMOVE_ITEM,
  SUB_QUANTITY
} from './constants';

export const addToCart = id => ({
  type: ADD_TO_CART,
  payload: id,
});

export const removeItem = id => ({
  type: REMOVE_ITEM,
  payload: id,
});

export const subtractQuantity = id => ({
  type: SUB_QUANTITY,
  payload: id,
});

export const addQuantity = id => ({
  type: ADD_QUANTITY,
  payload: id,
});
