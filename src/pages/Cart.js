import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Card from '../components/Card';
import { totalOrderAmount } from '../helpers/total';

const Cart = ({ myCart, shipping }) => {
  if (!myCart?.length) {
    return (
      <div className="container">
        <p className="nothing">Your cart is empty.</p>
        <p className="nothing">Go to <Link to='/'>Shop page</Link> to fill your cart</p>
      </div>
    )
  };

  return (
    <div className="container">
      <div className="cart-title">
        <h3 className="center"> You have ordered: </h3>
      </div>
      <div className="box">
        {
          myCart.map(({ id, img, title, desc, price, count }, index) => (
            <Card
              key={index}
              id={id}
              img={img}
              title={title}
              desc={desc}
              price={price}
              count={count}
            />
          ))
        }
      </div>
      <h3 className="center">Total order amount: {totalOrderAmount(myCart, shipping)}$ </h3>
      <h4 className="center">(including shipping in the amount of {shipping}$)</h4>
    </div>
  )
};

const mapStateToProps = state => ({
  myCart: state.myCart,
  shipping: state.shipping,
});

export default connect(mapStateToProps)(Cart);
