import React from 'react';
import { connect } from 'react-redux';
import Card from '../components/Card';

const Home = ({ products }) => {

  return (
    <div className="container">
      <h3 className="center">Our items</h3>
      <div className="box">
        {products?.map(({ id, img, title, desc, price, count }, index) => (
          <Card
            key={index}
            id={id}
            img={img}
            title={title}
            desc={desc}
            price={price}
            count={count}
          />
        ))}
      </div>
    </div>
  )
};

const mapStateToProps = state => ({
  products: state.products,
});

export default connect(mapStateToProps)(Home);
